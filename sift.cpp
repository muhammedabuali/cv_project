
/**
    Computer Vision Project 1 Milestone 1
    sift.cpp
    Purpose: Implementation of SIFT(Scale Invarient Feature Transform).





    @author Hesham Nabil , Muhammed Hassan and Dayna Hany
*/


#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <iostream>
#include <vector>
#include <cmath>

/*
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
*/

using namespace std;
using namespace cv;

const int DefaultOctaves = 4;
const int DefaultIntervals = 2;
const double InitialSigma = sqrt(2);
const double contrast_threshold = 0.03;
const double curvature_threshold = 10.0;
vector< vector <bool> > bools;
vector< vector <Mat> > pyramid;
Mat downSample(Mat&);


/**
    Calculates kernel size from sigma.

    @param sigma the sigma being used.
    @return an integer representing the kernel size.
*/
int getKernelSize(double sigma){
	return 2*round(3.5 * sigma)+1;
}


/**
    Builds a gaussian pyramid from an input matrix.

    @param image the image matrix to build the gaussian pyramid from.
    @param pyr the 2 dimensional vector of matrices where we build the gaussian pyramid for each octave.
    @param nOctaves the number of octaves to produce.
*/
void buildGaussianPyramid( Mat& image, vector< vector <Mat> >& pyr, int nOctaves ){

	double min, max;
	minMaxLoc(image, &min, &max);
	if (min<0|max>1)
	{
		cout << "image not normalized"<< endl;
	}

	Mat original;
	int k_size = getKernelSize(InitialSigma);
	Mat kernel = getGaussianKernel( k_size, sqrt(2), CV_64F );
	sepFilter2D( image, original, -1, kernel,kernel, Point(-1,-1),
	0,BORDER_CONSTANT);
	pyr = vector< vector <Mat> >(nOctaves, vector<Mat>(DefaultIntervals+3));
	pyr[0][0] = original;


	vector< vector <double> > sigma_pyr(nOctaves, vector<double>(DefaultIntervals+2));


	double sigma = InitialSigma;
	for (int oct = 0; oct < nOctaves; ++oct)
	{
		sigma = InitialSigma;
		cout << "builing octave "<< oct<< endl;
		for (int lev = 1; lev < DefaultIntervals+3; ++lev)
		{
			int k_size = getKernelSize(sigma);
			Mat kernel = getGaussianKernel( k_size, sigma, CV_64F );
			sigma = sqrt(2)* sigma;
			sepFilter2D( pyr[oct][lev-1], pyr[oct][lev], -1, kernel,kernel.t(), Point(-1,-1),
				0,BORDER_CONSTANT);
		}
		// prepare for next octave
		if (oct+1 < nOctaves)
		{
			pyr[oct+1][0] = downSample(pyr[oct][2]);
		}
	}
    pyramid = pyr;
}

/**
    Builds Difference-Of-Gaussian pyramid from an input gaussian pyramid.

    @param gauss_pyr the 2 dimensional vector of matrices where we build the gaussian pyramid for each octave.
    @return the 2d vector containing the DoG pyramid.
*/
vector< vector<Mat> > buildDogPyr(vector< vector<Mat> > gauss_pyr){

	vector< vector <Mat> > dog_pyr(gauss_pyr.size(), vector<Mat>(gauss_pyr[0].size()-1));

	for (int i = 0; i < gauss_pyr.size(); ++i)
	{
		for (int j = 1; j < gauss_pyr[0].size(); ++j)
		{
			dog_pyr[i][j-1]= gauss_pyr[i][j] - gauss_pyr[i][j-1];
		}
	}
	return dog_pyr;
}

/**
    Doubles the image in width and height.

    @param image the image to double.
*/
Mat upSample(Mat& image){
    Mat res = Mat(image.rows , image.cols*2 , image.type());
    for(int i = 0 ; i < image.cols ; ++i)
    {
        image.col(i).copyTo(res.col(2*i));
        image.col(i).copyTo(res.col(2*i + 1));
    }
    Mat result = Mat(res.rows*2 , res.cols , res.type());
     for(int i = 0 ; i < image.rows ; ++i)
    {
        res.row(i).copyTo(result.row(2*i));
        res.row(i).copyTo(result.row(2*i + 1));
    }
    return result;
}

/**
    Shrinks the image in width and height by a factor of 2.

    @param image the image to shrink.
*/
Mat downSample(Mat& image){
	Mat tmp = Mat(image.rows, image.cols/2, image.type());
	for (int i = 0; i < image.cols/2; ++i)//sample columns
	{
		image.col(i*2).copyTo(tmp.col(i));
	}
	Mat out = Mat(tmp.rows/2, tmp.cols, tmp.type());
	for (int i = 0; i < tmp.rows/2; ++i)//sample rows
	{
		tmp.row(i*2).copyTo(out.row(i));
	}
	return out;
}

/**
    Computes magnitude and orientation of a keypoint.

    @param K the keypoint on which to compute and set the orientation and magnitude.
*/
void computeOrientationHist( KeyPoint& K){
    Mat temp = pyramid[K.octave][K.class_id];
    double dx = temp.at<double>(K.pt.x+1 , K.pt.y) - temp.at<double>(K.pt.x-1 , K.pt.y);
    double dy = temp.at<double>(K.pt.x , K.pt.y+1) - temp.at<double>(K.pt.x , K.pt.y-1);
    K.response = sqrt((dx*dx)+(dy*dy));
    K.angle = atan(dy/dx);
}

/**
   Filters points that are not suitable as keypoints.

    @param image the image to eliminate points from.
    @param curv_thr the maximum allowed curvature threshold.
*/
void cleanPoints( Mat& image, int curv_thr )
{
bools = vector< vector <bool> >(image.rows, vector<bool>(image.cols));
double f;
double dxx;
double dyy;
double dxy;
double curv_ratio;
double det_H;
for (int i = 1; i < image.rows-1; ++i)
	{
        for(int j = 1 ; j< image.cols-1 ; ++j){
            f = abs(image.at<double>(i,j));
            bools[i][j] = true;

            if(  f < contrast_threshold){
                bools[i][j] = false;

            }
            dxx = (-2 * image.at<double>(i,j)) + image.at<double>(i-1,j) + image.at<double>(i+1,j);
            dyy = (-2 * image.at<double>(i,j)) + image.at<double>(i,j-1) + image.at<double>(i,j+1);
            dxy = image.at<double>(i-1,j-1) - image.at<double>(i+1,j-1) - image.at<double>(i-1,j+1) + image.at<double>(i+1,j+1);
            dxy = dxy/4;
            det_H = dxx*dyy - (dxy*dxy);
            curv_ratio = ((dxx+dyy)*(dxx+dyy)) / det_H;
            if(curv_ratio > curv_thr){
                bools[i][j] = false;
            }


	        }

		}
}

/**
   Computes extrema values for points across scales in octaves.

    @param dog_pyr the DoG pyramid on which to calculate the scale-space extremas.
    @param keypoints the keypoints to be calculated.
*/
void getScaleSpaceExtrema(vector< vector<Mat> >& dog_pyr, vector<KeyPoint>& keypoints){
	int old = 0;
	for (int i = 0; i < dog_pyr.size(); ++i)//over octaves
	{
		//cout<< "octave" << i << endl;
		for (int j = 1; j < dog_pyr[0].size()-1; ++j)//over scales
		{
			//cout<< "scale" << j << endl;
			cleanPoints(dog_pyr[i][j] , 10);
			for (int x = 1; x < dog_pyr[i][j].rows-1; ++x)//over rows
			{
				for (int y = 1; y < dog_pyr[i][j].cols-1; ++y)//over cols
				{
					if(bools[x][y] == false){
					   continue;
					}
					double cur = dog_pyr[i][j].at<double>(x,y);
					Mat m = dog_pyr[i][j](Range(x-1,x+2),Range(y-1,y+2));
					double min,max;
					minMaxLoc(m,&min,&max);
					bool is_min = false;
					if (min == cur)
					{
						is_min = true;
					}else if(max != cur){
						continue;
					}

					Mat up = dog_pyr[i][j-1](Range(x-1,x+2),Range(y-1,y+2));
					minMaxLoc(up,&min,&max);
					if (min < cur && is_min)
					{
						continue;
					}else if (max > cur && !is_min)
					{
						continue;
					}
					Mat down = dog_pyr[i][j+1](Range(x-1,x+2),Range(y-1,y+2));
					minMaxLoc(down,&min,&max);
					if (min < cur && is_min)
					{
						continue;
					}else if (max > cur && !is_min)
					{
						continue;
					}
							//cout <<is_min << "x " << x <<" y " << y<< endl;
                    KeyPoint k;
	                k.pt = Point2f(x,y);
	                k.octave = i;
	                k.class_id = j;
	                computeOrientationHist(k);
	                keypoints.push_back(k);
				}
			}
			cout << "deteced " << keypoints.size() - old<<endl;
			old = keypoints.size();
		}
	}
}

/**
	Read image as gray.

	@param file_path the path to the image file
*/
Mat readGrayImage(string file_path){
	Mat img = imread(file_path,CV_LOAD_IMAGE_GRAYSCALE);
	Mat norm;
	img.convertTo( norm , CV_64F, 1.0/255.0);
	return norm;
}

/**
   Displays the computed keypoints.

    @param img the image to display keypoints on.
    @param keypoints the keypoints to display.
*/
void displayPoints (Mat& img , vector<KeyPoint>& keypoints ){
    namedWindow( "Original Fish", WINDOW_AUTOSIZE );
	imshow("Original Fish" , img);
    for(int i = 0; i < keypoints.size() ; ++i){
        KeyPoint k = keypoints[i];
        int x = k.pt.x * pow (2.0, k.octave);
        int y = k.pt.y * pow (2.0, k.octave);
        circle(img, Point(y,x), 3, Scalar(255,255,0), -1, 8, 0);

    }
    namedWindow( "Altered Lena", WINDOW_AUTOSIZE );
	imshow("Altered Lena" , img);
}

void findSiftInterestPoint( Mat& image, vector<KeyPoint>& keypoints){
    image = upSample(image);
    Mat norm;
	image.convertTo( norm , CV_64F, 1.0/255.0);
	vector< vector <Mat> > pyr;
	buildGaussianPyramid(norm, pyr, 4);
	vector< vector<Mat> > dog = buildDogPyr(pyr);
	getScaleSpaceExtrema(dog, keypoints);

}

int main(int argc, char const *argv[])
{
//cout << "hello" << endl;
	Mat img = imread("lena.jpg",CV_LOAD_IMAGE_GRAYSCALE);
	Mat image = imread("lena.jpg",CV_LOAD_IMAGE_COLOR);
	image = upSample(image);
    vector< KeyPoint > points;
    findSiftInterestPoint(img , points);
    displayPoints(image , points);
    cout<< "KeyPoint count" << points.size() << endl;


	//Mat m = upSample(img);
	//namedWindow( "Display window", WINDOW_AUTOSIZE );
	//imshow("Display window" , m);
	//cout<< "rows" << img.rows << "cols" << img.cols << endl;
	//cout<< "rows" << m.rows << "cols" << m.cols << endl;

	//cout << "kp_count " << points.size() << endl;
	//cout << dog[4][4] << endl;
	waitKey(0);
	return 0;}

/*
TEST_CASE( "SIFT methods") {
	Mat img = readGrayImage("data/Happy_fish.jpg");
	
	vector< vector <Mat> > pyr;
	buildGaussianPyramid(img, pyr, 4);
	vector< vector<Mat> > dog = buildDogPyr(pyr);

	vector< KeyPoint > points;
	getScaleSpaceExtrema(dog, points);
	

 	SECTION(" build gaussian pyramid"){
 		
 		REQUIRE(pyr.size() == 4);
 		REQUIRE(pyr[0].size() == 5);
 		REQUIRE(pyr[3][4].size() == Size(pyr[0][0].rows / (2*4) ,pyr[0][0].cols / (2*4)));

 		double min, max;
 		for (int i = 0; i < 4; ++i)
 		{
 			for (int j = 0; j < 5; ++j)
 			{
 				minMaxLoc(pyr[i][j], &min, &max);
		 		REQUIRE(min >= 0);
		 		REQUIRE(max <= 1);
 			}
 		}
	}

	SECTION("build DOG pyramid"){
		
		REQUIRE(dog.size() == 4);
 		REQUIRE(dog[0].size() == 4);
 		REQUIRE(dog[3][3].size() == pyr[3][0].size());

 		double min, max;
 		for (int i = 0; i < 4; ++i)
 		{
 			for (int j = 0; j < 4; ++j)
 			{
 				minMaxLoc(dog[i][j], &min, &max);
		 		REQUIRE(min >= -0.3);
		 		REQUIRE(max <= 0.3);
 			}
 		}
	}
}

TEST_CASE("clean points"){
	Mat img = Mat::ones(10, 10, CV_64F);
	Mat ones = Mat::ones(10,5,CV_64F) + Mat::ones(10,5,CV_64F);
	for (int i = 0; i < 1; ++i)
	{
		ones.col(i).copyTo(img.col(i+5));
	}
	cleanPoints(img, 10.0);
	cout << img << endl;
	for (int i = 0; i < 10; ++i)
	{
		for (int j = 0; j < 10; ++j)
		{
			cout << bools[i][j];
		}
		cout << endl;
	}
	int c = 4;
	for (int i = 0; i < 10; ++i)
	{
		REQUIRE(bools[i][c] == false);
	}
	c = 6;
	for (int i = 0; i < 10; ++i)
	{
		REQUIRE(bools[i][c] == false);
	}
}

TEST_CASE("upsample and downSample"){
	Mat img = Mat::ones(128,128,CV_64F);
	Mat down = downSample(img);
	REQUIRE(down.size() == Size(64,64));
}
*/
