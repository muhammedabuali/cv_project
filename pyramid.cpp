#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <vector>
#include <cmath>

using namespace cv;
using namespace std;


int main( int argc, char** argv )
{
    char* imageName = argv[1];

    Mat image;
    // convert to gray scale
    image = imread( imageName, CV_LOAD_IMAGE_GRAYSCALE );

    if( argc != 2 || !image.data )
    {
        cout << " No image data" << endl;
        return -1;
    }

    Mat img;
     // normalize
    image.convertTo(img,CV_32F, 1.0/255.0);
    
    Mat blurred;
    double initial_sigma = sqrt(2);
    double sigma = initial_sigma;

    GaussianBlur(img, blurred, Size(5,5),sigma);

/*    namedWindow( imageName, CV_WINDOW_AUTOSIZE );
    imshow( imageName, img );
    namedWindow( "blurred", CV_WINDOW_AUTOSIZE );
    imshow( "blurred", blurred);*/

    vecctor<vector<Mat>> pyramid(4, vector<Mat>(5));
    pyramid[0][0] = blurred;
    Mat prev = blurred;
    for (int i =0; i < 4; i++){// loop over octaves
        for (int j =1; j < 5; j++){// loop over gaussians
            Mat tmp;
            GaussianBlur(prev, tmp, Size(5,5), sigma);
            sigma += initial_sigma;
            pyramid[i][j]= tmp;
            prev = tmp;
        }
    }
    waitKey(0);

    return 0;
}

void buildGaussianPyramid( Mat& image, vector< vector <Mat> >& pyr, int nOctaves ){
    //TODO
}

Mat downSample(Mat& image){

}
